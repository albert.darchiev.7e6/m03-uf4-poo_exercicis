private class Pasta (val name: String,
             val price: Double,
             val weigth: Double,
             val calories: Double )
fun main() {
    val croissant = Pasta("croissant", 1.45, 80.5,405.6)
    val ensaimada = Pasta("ensaimada", 1.05, 35.2, 184.5)
    val donut = Pasta("donut", 0.65, 40.0,267.2)
    val pastas = listOf(croissant, ensaimada,donut)
    for (pasta in pastas){
        println("${pasta.name.uppercase()} \nPRICE: ${pasta.price}€ \nPES: ${pasta.weigth} \nCALORIES: ${pasta.calories}Kcal \n" )
    }
}