private class Pasta2 (val name: String, val price: Double, val weigth: Double, val calories: Double )
private class Beguda(val name: String, var price: Double, val sugar: Boolean){
    init {
        if (sugar) price *= 1.1
    }
}
fun main() {
    val croissant = Pasta2("croissant", 1.45, 80.5,405.6)
    val ensaimada = Pasta2("ensaimada", 1.05, 35.2, 184.5)
    val donut = Pasta2("donut", 0.65, 40.0,267.2)
    val pastas = listOf(croissant, ensaimada,donut)
    for (pasta in pastas){
        println("${pasta.name.uppercase()} \nPRICE: ${pasta.price}€ \nCALORIES: ${pasta.calories}Kcal \n" )
    }

    val aigua = Beguda("aigua", 1.00, false)
    val cafe = Beguda("café tallat", 1.35, false)
    val te = Beguda("té vermell", 1.50, false)
    val cola = Beguda("cola", 1.65, true)
    val begudes = listOf(aigua, cafe, te, cola)
    for (beguda in begudes){
        println("${beguda.name.uppercase()} \nPRICE: ${beguda.price}€ \nSUCRE: ${beguda.sugar}\n")
    }
}