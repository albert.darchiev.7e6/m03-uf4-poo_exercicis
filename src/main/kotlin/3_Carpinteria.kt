import java.util.Scanner

class Taulell(var price: Int, val width: Int, val heigth: Int){
    init { price *= width*heigth }
}
class LListo(var price: Int, val length: Int){
    init { price *= length }
}

val scanner = Scanner(System.`in`)

fun main(){
    var totalPrice = 0
    repeat(scanner.nextInt()+1) {
        val input = scanner.nextLine().split(" ")
        if (input[0] == "Taulell"){
            var item = Taulell(input[1].toInt(), input[2].toInt(), input[3].toInt())
            totalPrice += item.price
        }
        else if(input[0] == "Llistó"){
            var item = LListo(input[1].toInt(), input[2].toInt())
            totalPrice += item.price
        }
    }
    println("El preu total es de: ${totalPrice}€")
}