//definició dels colors que farem servir, agafant els backgrounds ANSI, i establint-los com a constants globals
const val ANSI_RESET = "\u001B[0m"
const val ANSI_BLACK = "\u001B[40m"
const val ANSI_RED = "\u001B[41m"
const val ANSI_GREEN = "\u001B[42m"
const val ANSI_YELLOW = "\u001B[43m"
const val ANSI_BLUE = "\u001B[44m"
const val ANSI_PURPLE = "\u001B[45m"
const val ANSI_CYAN = "\u001B[46m"
const val ANSI_WIHTE = "\u001B[47m"

val colors = arrayOf(ANSI_BLACK, ANSI_WIHTE, ANSI_RED, ANSI_RED, ANSI_YELLOW, ANSI_BLUE, ANSI_PURPLE, ANSI_CYAN)

class lampada(ID: Int){
    val id = ID
    var turnedOn = false
    var color = 1
    var intensitat = 1
    var intensitatPujada=true


    //    var offColor =
    fun turnOn(){
        turnedOn = true
        intensitat=1
        intensitatPujada=true
        printState()
    }
    fun turnOff(){
        turnedOn = false
        intensitat=0
        intensitatPujada=false
        printState()
    }
    fun changeColor(){
        if (color<colors.size-1 && color != 0) color++
        else if(color==colors.size-1) color=1
        printState()
    }
    fun changeIntensity(){
        if (intensitatPujada) intensitat++
        else if (intensitatPujada && intensitat==5){
            intensitatPujada=false
            intensitat--
        }
        else if (!intensitatPujada && intensitat>1) intensitat--
        else if (!intensitatPujada && intensitat==1){
            intensitatPujada = true
            intensitat++
        }
        printState()
    }
    fun printState(){
        println("Color: ${colors[color]}    $ANSI_RESET - intensitat $intensitat  -Encés: $turnedOn")
    }
}

fun main(){
    var lampada1 = lampada(0)
    var lampada2 = lampada(1)

    printLampID(lampada1)
    lampada1.turnOn()
    repeat(3) { lampada1.changeColor() }
    do { lampada1.changeIntensity()
    }while (lampada1.intensitat != 5)

    printLampID(lampada2)
    lampada2.turnOn()
    repeat(2){lampada2.changeColor()}
    do { lampada2.changeIntensity()
    }while (lampada2.intensitat != 5)
    lampada2.turnOff()
    lampada2.changeColor()
    lampada2.turnOn()
    lampada2.changeColor()
    do { lampada2.changeIntensity()
    }while (lampada2.intensitat != 5)

}
fun printLampID(lamp: lampada){
    println("\n -- LAMPADA ID: ${lamp.id} --")
}