import java.io.File

private class MechanicArmApp (var status: Boolean,
                           var angle: Double,
                           var altitude: Double)

fun main(){
    val file = File("src/main/kotlin/5_MechanicArmInstructions").readLines()
    val mechanicArm = MechanicArmApp(false, 0.0, 0.0)
    file.forEach {
        //println(it)
        if ("toggle" in it) {
            mechanicArm.status = !mechanicArm.status
        }
        else if ("Altitude" in it || mechanicArm.altitude + takeValue(it) <= 30) {
            if (mechanicArm.status){
                mechanicArm.altitude += takeValue(it)
            }
        }
        else if ("Angle" in it || mechanicArm.angle + takeValue(it) <= 360) {
            if (mechanicArm.status){
                mechanicArm.angle += takeValue(it)
            }
        }
        else println("ERROR")
        println("openANGLE= ${mechanicArm.angle}, ALTITUDE= ${mechanicArm.altitude}, turnedON= ${mechanicArm.status}")
    }
}
fun takeValue(input: String): Double {
    val start = input.indexOf("(") + 1
    val end = input.indexOf(")", start)
    val value = input.substring(start, end)
    return value.trim().toDouble()
}